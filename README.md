# README #

This README would normally document whatever steps are necessary to get your application up and running.
http://ubuntuhandbook.org/index.php/2013/07/how-to-convert-png-to-jpg-on-ubuntu-via-command/

## What is this repository for?
* for classification using mobilenet v2 CNN network

  ```Shell
  $SQDT_ROOT/training/
                    |->label/
                    |    L-> 00****.txt
                    |->image/
                    |    L-> 00****.png
                    L->calib/
                    |    L-> 00****.txt
                    L->velodyne/
                         L-> 
  ```


## 1 AID Munich Task

### 1.1 project structure

The project is training files, scripts and model are structure into different folders
  
  Folder | Description
  :---: | ---
  `training` | Contains KITTI datasets for training including image, label, calib, and velodyne files
  `script` | Contains python scripts for data load, data write and data prepration
  `perception` | Description about task and access to AWS instance
  `model` | imagenet pre-trained model of mobilenet v2
  
### 1.2 file description

Description of shell and python file to prepare data and start training
  
  Files | Description
  :---: | ---
  `gen_data.py` | Generates training targets/labels for ROI for Classification
  `eval_image.py` | Demo script for an image
  `create_lmdb.sh` | Converting image files and label into lmdb
  `train.sh` | Start training using caffe
  `test.sh` | Test trained model
