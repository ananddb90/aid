#!/bin/sh
if ! test -f model/mobilenet_v2_train.prototxt ;then
	echo "error: example/mobilenet_v2_train.prototxt does not exist."
	echo "please use check the path."
        exit 1
fi
mkdir -p snapshot
#CAFFE_ROOT = '/home/anand/Desktop/anand/py-R-FCN/caffe'
/home/anand/Desktop/anand/ssd-caffe/build/tools/caffe train -solver="model/solver.prototxt" -weights="model/mobilenet_v2.caffemodel" -gpu 0,1,2 
