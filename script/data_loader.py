#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Provides 'Parser', which loads and parses raw data."""

from __future__ import absolute_import, print_function
import glob
import os
try:
    from itertools import izip
except:
    izip = zip
import utils
import pdb

class Parser(object):
    """Load and parse data into a usable format."""

    def __init__(self, data_path='training/', training=True):
        """
        Set the path and pre-load calibration data.
        Assumes the following folder organization:

        data_path
        └─── image
            │   *.png (Image files, ex:000000.png)
        └─── calib
            |   *.txt (Calibration files, ex:000000.txt)
        └─── velodyne
            |   *.bin (Label files, ex:000000.bin)
        └─── label (if training is True)
            |   *.txt (Label files, ex:000000.txt)
        """
        self._training = training
        self.data_path = data_path
        self.image_path = os.path.join(self.data_path, 'image')
        self.velo_path = os.path.join(self.data_path, 'velodyne')
        self.calib_path = os.path.join(self.data_path, 'calib')

        self._image_files = sorted(
            glob.glob(os.path.join(self.image_path, '*.png')))
        self._velo_files = sorted(
            glob.glob(os.path.join(self.velo_path, '*.bin')))
        self._calib_files = sorted(
            glob.glob(os.path.join(self.calib_path, '*.txt')))

        if self._training:
            self.label_path = os.path.join(self.data_path, 'label')
            self._label_files = sorted(
                glob.glob(os.path.join(self.label_path, '*.txt')))
        # source directories and check consistency
        self._check_name_consistency()

    @property
    def name(self):
        """
        Generator to retrieve names of samples.
        """
        return utils.get_names(self._image_files)

    @property
    def cam(self):
        """
        Generator to read image files for camera (RGB left).
        """
        return utils.get_images(self._image_files)

    @property
    def velo(self):
        """
        Generator to read velodyne scan data from binary files.
        Each scan is a Nx4 array of [x,y,z,reflectance].
        x,y,z are in units of meters.
        """
        return utils.get_velo_scans(self._velo_files)

    @property
    def label(self):
        """
        Generator to read label files corresponding to image/lidar pairs.
        For each label file, it will return a list of dictionaries.
        Each entry in the list contains an object with certain attributes:

        #Values     Key        Description
        ----------------------------------------------------------------------------
        1           type        Describes the type of object: 'Car', 'Van', 'Truck',
                                'Pedestrian', 'Person_sitting', 'Cyclist', 'Tram',
                                'Misc' or 'DontCare'

        1           truncated   Float from 0 (non-truncated) to 1 (truncated), where
                                truncated refers to the object leaving image boundaries

        1           occluded    Integer (0,1,2,3) indicating occlusion state:
                                0 = fully visible, 1 = partly occluded
                                2 = largely occluded, 3 = unknown

        1           alpha       Observation angle of object, ranging [-pi..pi]

        4           bbox        2D bounding box of object in the image (0-based index):
                                contains left, top, right, bottom pixel coordinates

        3           dimensions  3D object dimensions: height, width, length (in meters)

        3           location    3D object location x,y,z in camera coordinates (in meters)

        1           rotation_y  Rotation ry around Y-axis in camera coordinates [-pi..pi]

        Here, 'DontCare' labels denote regions in which objects have not been labeled,
        for example because they have been too far away from the laser scanner. To
        prevent such objects from being counted as false positives our evaluation
        script will ignore objects detected in don't care regions of the test set.
        You can use the don't care labels in the training set to avoid that your object
        detector is harvesting hard negatives from those areas, in case you consider
        non-object regions from the training images as negative examples.
        """
        return utils.get_label(self._label_files)

    @property
    def calib(self):
        """
        Generator to read calibration files and return them as a dictionary of arrays.

        The coordinates in the camera coordinate system can be projected in the image
        by using the 3x4 projection matrix in the calib folder, where for the left
        color camera for which the images are provided, P2 must be used. The
        difference between rotation_y and alpha is, that rotation_y is directly
        given in camera coordinates, while alpha also considers the vector from the
        camera center to the object center, to compute the relative orientation of
        the object with respect to the camera. For example, a car which is facing
        along the X-axis of the camera coordinate system corresponds to rotation_y=0,
        no matter where it is located in the X/Z plane (bird's eye view), while
        alpha is zero only, when this object is located along the Z-axis of the
        camera. When moving the car away from the Z-axis, the observation angle
        will change.

        To project a point from Velodyne coordinates into the left color image,
        you can use this formula: x = P2 * R0_rect * Tr_velo_to_cam * y

        Note: All matrices are stored row-major, i.e., the first values correspond
        to the first row. R0_rect contains a 3x3 matrix which you need to extend to
        a 4x4 matrix by adding a 1 as the bottom-right element and 0's elsewhere.
        Tr_xxx is a 3x4 matrix (R|t), which you need to extend to a 4x4 matrix
        in the same way!
        """
        return utils.get_calib(self._calib_files)

    @property
    def all(self):
        """
        Generator to read matching camera, velodyne and calibration files.
        If parser object has training = True, it returns:
            cam, velo, calib, label data
        If parser object has training = False, it returns:
            cam, velo, calib data
        """
        if self._training:
            return izip(self.cam, self.velo, self.calib, self.label)
        else:
            return izip(self.cam, self.velo, self.calib)

    @property
    def all_with_name(self):
        """
        Generator to read matching camera, velodyne and calibration files.
        Dmitry Khizbullin: added file name to be able to know name of a sample.
        If parser object has training = True, it returns:
            name, cam, velo, calib, label data
        If parser object has training = False, it returns:
            name, cam, velo, calib data
        """
        if self._training:
            return izip(self.name, self.cam, self.velo, self.calib, self.label)
        else:
            return izip(self.name, self.cam, self.velo, self.calib)

    def __len__(self):
        """Return length of iterator"""
        return len(self._image_files)

    def _check_name_consistency(self):
        if self._training:
            files = izip(self._image_files,
                         self._calib_files,
                         self._velo_files,
                         self._label_files)
        else:
            files = izip(self._image_files,
                         self._calib_files,
                         self._velo_files)
        for group in files:
            assert(
                len(set(os.path.basename(f).split('.')[0] for f in group)) <= 1)
