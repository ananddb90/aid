import data_loader as dl
import data_writer as dw
import utils as ut
import pdb



import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np
import os


path = '/media/uidq7257/Anand/training/'
out = '/media/uidq7257/Anand/training/output/'
cls_dic = {'Car': 0, 'Van': 1, 'Truck': 2, 'Pedestrian': 3, 'Person_sitting': 4, 'Cyclist': 5, 'Tram': 6, 'Misc':7 ,'DontCare':7,'None':7}
pars = dl.Parser(path, True)
label = pars.label
files = pars.name
cls = []
bbox = []



def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)

    except OSError:
        print ('Error: Creating directory. ' + directory)


def create_traintxt(path, file_path, cls):
    train_file = path + 'train.txt'
    with open(train_file, 'ab+') as f:
        f.write('{}\t{}\n'.format(file_path,cls))
        f.close()


def create_valtxt(path, file_path, cls):
    val_file = path + 'val.txt'
    with open(val_file, 'ab+') as f:
        f.write('{}\t{}\n'.format(file_path,cls))
        f.close()



for i in range(pars.__len__()):
    _list = label.next()
    im = Image.open(files.next())
    fig,ax = plt.subplots(1)
    ax.imshow(im)
    for j in range(len(_list)):
        cls.append(cls_dic.get(_list[j].get('type')))
        bbox.append(_list[j].get('bbox').astype(int))
        #print cls, bbox
        bb = _list[j].get('bbox').astype(int)
        rect = patches.Rectangle((bb[0], bb[1]),bb[2]-bb[0], bb[3]-bb[1],linewidth=1,edgecolor='r',facecolor='none')
        ax.add_patch(rect)
        ax.text(bb[0], bb[1], _list[j].get('type'), fontsize=14, bbox=dict(facecolor='red', alpha=0.5))

        img = im.crop((bb[0], bb[1], bb[2], bb[3]))
        createFolder(path + 'output/')
        train_img = path + 'output/'

        file_path = train_img + str(i)+ '_' + str(j) + '_' + str(cls_dic.get(_list[j].get('type'))) + '.png'
        img.save(file_path)
        if np.random.rand() <= 0.8:
            create_traintxt(path, file_path, cls_dic.get(_list[j].get('type')))
        else:
            create_valtxt(path, file_path, cls_dic.get(_list[j].get('type')))

    #plt.show()
    #pdb.set_trace()

