"""The data layer used during training to train a network.
RoIDataLayer implements a Caffe Python layer.
"""

import script.data_loader as dl
import script.data_writer as dw
import script.utils as ut
import pdb

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np

import caffe
import yaml
from multiprocessing import Process, Queue

CLASSES = {'Car': 0, 'Van': 1, 'Truck': 2, 'Pedestrian': 3, 'Person_sitting': 4, 'Cyclist': 5, 'Tram': 6, 'Misc': 7}


class RoIDataLayer(caffe.Layer):
    """data layer used for training."""

    def setup(self, bottom, top):
        """ Checks the correct number of bottom inputs."""
        self.pars = dl.Parser('training/', True)
        self.label = pars.label
        self.files = pars.name

        if len(bottom) != 1:
            raise Exception("needs input data path")


    def forward(self, bottom, top):
        """Get blobs and copy them into this layer's top blob vector."""
        _list = self.label.next()
        print _list[:].get('bbox')
        print CLASSES.get(_list[:].get('type'))

        top[0].reshape(*_list[:].get('bbox').shape)
        top[0].data[...] = _list[:].get('bbox')

        top[1].reshape(*_list[:].get('bbox').shape)
        top[1].data[...] = CLASSES.get(_list[:].get('type'))
        pdb.set_trace()



    def backward(self, top, propagate_down, bottom):
        """This layer does not propagate gradients."""
        pass

    def reshape(self, bottom, top):
        """Reshaping happens during the call to forward."""
        pass


def visualization():
    """draw bbox on training img"""

