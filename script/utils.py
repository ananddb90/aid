#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Provides helper methods for loading and parsing data."""

from __future__ import absolute_import, print_function
import numpy as np
from PIL import Image


def get_names(imfiles):
    """Generator to get names of samples."""
    for filename in imfiles:
        yield filename


def get_images(imfiles):
    """Generator to read image files into arrays."""
    for filename in imfiles:
        yield np.asarray(Image.open(filename), np.uint8)


def get_velo_scans(velo_files):
    """Generator to parse velodyne binary files into arrays."""
    for filename in velo_files:
        scan = np.fromfile(filename, dtype=np.float32)
        yield scan.reshape((-1, 4))


def get_calib(calib_files):
    """Generator to parse calibration text files into a dictionary."""
    for filename in calib_files:
        data = {}
        with open(filename, 'r') as f:
            lines = f.readlines()
            for i in range(7):
                key, value = lines[i].split(':', 1)
                data[key] = np.array([float(x) for x in value.split()])
        yield data


def get_label(label_files):
    """Generator to parse label text files into a dictionary."""
    for filename in label_files:
        data = []
        with open(filename, 'r') as f:
            for line in f.readlines():
                values = line.split()
                assert len(values) == 15
                obj = {
                    'type': str(values[0]),
                    'truncated': float(values[1]),
                    'occluded': int(values[2]),
                    'alpha': float(values[3]),
                    'bbox': np.array(values[4:8], dtype=float),
                    'dimensions': np.array(values[8:11], dtype=float),
                    'location': np.array(values[11:14], dtype=float),
                    'rotation_y': float(values[14]),
                }
                data.append(obj)
        yield data


def rotx(t):
    """Rotation about the x-axis."""
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[1,  0,  0],
                     [0,  c, -s],
                     [0,  s,  c]])


def roty(t):
    """Rotation about the y-axis."""
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c,  0,  s],
                     [0,  1,  0],
                     [-s, 0,  c]])


def rotz(t):
    """Rotation about the z-axis."""
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c, -s,  0],
                     [s,  c,  0],
                     [0,  0,  1]])


def transform_from_rot_trans(R, t):
    """Transforation matrix from rotation matrix and translation vector."""
    R = R.reshape(3, 3)
    t = t.reshape(3, 1)
    return np.vstack((np.hstack([R, t]), [0, 0, 0, 1]))