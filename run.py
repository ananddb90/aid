'''
main docuemnt for : data prepration - training - testing

'''

from pylab import *
import caffe
import sys
import os
from random import shuffle
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pdb
sys.path.insert(0,os.path.join(os.getcwd() + '/script/'))
import data_loader as dl
import data_writer as dw
import utils as ut
from PIL import Image
from subprocess import call




CLASSES = {'Car': 0, 'Van': 1, 'Truck': 2, 'Pedestrian': 3, 'Person_sitting': 4, 'Cyclist': 5, 'Tram': 6, 'Misc':7 ,'DontCare':7,'None':7}

def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Train a AID network')
    parser.add_argument('--gpu', dest='gpu_id',
                        help='GPU device id to use [0]',
                        default=0, type=int)
    parser.add_argument('--solver', dest='solver',
                        help='solver prototxt',
                        default='model/mobilenet_v2_train.prototxt', type=str)
    parser.add_argument('--iters', dest='max_iters',
                        help='number of iterations to train',
                        default=40000, type=int)
    parser.add_argument('--weights', dest='model',
                        help='initialize with pretrained model weights',
                        default='model/mobilenet_v2.caffemodel', type=str)
    parser.add_argument('--data_path', dest='data_path',
                        help='path to training folder',
                        default=os.path.join(os.getcwd() + '/training/'), type=str)
    parser.add_argument('--mode', dest='mode',
                        help='test or train',
                        default='train', type=str)
    parser.add_argument('--RNG_SEED', dest='RNG_SEED',
                        help='random seeds',
                        default=1234, type=int)
    if len(sys.argv) < 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args


def _createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
            print 'folder created ...'
    except OSError:
        print ('Error: Creating directory. ' + directory)

def _silentremove(filename):
    if os.path.exists(filename):
	os.remove(filename)


def _create_traintxt(path, file_path, cls):
    print 'creating train files...'
    train_file = path + 'train.txt'
    with open(train_file, 'ab+') as f:
        f.write('{}\t{}\n'.format(file_path,cls))
        f.close()


def _create_valtxt(path, file_path, cls):
    print 'creating validation files...'
    val_file = path + 'val.txt'
    with open(val_file, 'ab+') as f:
        f.write('{}\t{}\n'.format(file_path,cls))
        f.close()

def _create_testtxt(path, file_path, cls):
    print 'creating testing files...'
    test_file = path + 'test.xt'
    with open(val_file, 'ab+') as f:
        f.write('{} {}\n'.format(file_path,cls))
        f.close()


def _train_data_prep(path, pars, label, files):

    _silentremove(path + 'train.txt')
    _silentremove(path + 'val.txt')
    _silentremove(path + 'test.txt')
    for i in range(pars.__len__()):
        _list = label.next()
        im = Image.open(files.next())
        for j in range(len(_list)):
            bb = _list[j].get('bbox').astype(int)
	    img = im.crop((bb[0], bb[1], bb[2], bb[3]))
	    _createFolder(path + 'train_img/')
            train_img = path + 'train_img/'
	    file_path = train_img + str(i)+ '_' + str(j) + '_' + str(CLASSES.get(_list[j].get('type'))) + '.png'
	    print file_path
            img.save(file_path)
	    if np.random.rand() <= 0.8:
            	_create_traintxt(path, file_path, CLASSES.get(_list[j].get('type')))
	    else:
            	_create_valtxt(path, file_path, CLASSES.get(_list[j].get('type')))


def _test_data_prep(path, pars, label, files):
    for i in range(pars.__len__()):
        _list = label.next()
        im = Image.open(files.next())
        for j in range(len(_list)):
            bb = _list[j].get('bbox').astype(int)
	    img = im.crop((bb[0], bb[1], bb[2], bb[3]))
	    _createFolder(path + 'test_img/')
            train_img = path + 'test_img/'
	    file_path = train_img + str(i)+ '_' + str(j) + '_' + str(CLASSES.get(_list[j].get('type'))) + '.png'
	    print file_path
            img.save(file_path)
	    _create_testtxt(path, file_path, CLASSES.get(_list[j].get('type')))



def train_net():
    for it in range(niter):
        solver.step(1)
        solver.test_nets[0].forward(start='conv1_1')


def visualization():
	print 'debugging bbox on image'


def main():
    args = parse_args()
    pdb.set_trace()

    np.random.seed(args.RNG_SEED)
    caffe.set_random_seed(args.RNG_SEED)

    # set up caffe
    caffe.set_mode_gpu()
    caffe.set_device(args.gpu_id)

    pars = dl.Parser(args.data_path, True)
    label = pars.label
    files = pars.name

    #select training or testing mode
    if args.mode == 'train' :
        #data_prep + training
        _train_data_prep(args.data_path, pars, label, files)
        #solver = caffe.SGDSolver(args.solver)
        #solver.net.copy_from(args.model)
        #train_net(solver)

    if args.mode == 'test' :
        #data_prep + testing
        test_data_prep()
        #test_net()

if __name__ == '__main__':
    main()
